
//
//  ViewControllerMap.swift
//  loeffel_app
//
//  Created by Mario Hoffmann on 26.02.15.
//  Copyright (c) 2015 Mario Hoffmann. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewControllerMap: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var mapSegment: UISegmentedControl!
    @IBAction func maptypeChange(sender: AnyObject) {
        switch mapSegment.selectedSegmentIndex {
        case 0:
            map.mapType = MKMapType.Standard;
        case 1:
            map.mapType = MKMapType.Satellite;
        case 2:
            map.mapType = MKMapType.Hybrid;
        default:
            map.mapType = MKMapType.Standard;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let location = CLLocationCoordinate2D(latitude: 51.326625, longitude: 12.373326)
        let span = MKCoordinateSpanMake(0.009, 0.009)
        let region = MKCoordinateRegion(center: location, span: span)
        map.setRegion(region, animated: true)
        
        //3
        let annotation = MKPointAnnotation()
        annotation.setCoordinate(location)
        annotation.title = "Die Löffelfamilie"
        annotation.subtitle = "Leuchtreklame"
        map.addAnnotation(annotation)

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}