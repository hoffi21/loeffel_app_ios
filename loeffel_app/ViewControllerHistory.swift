//
//  ViewControllerHistory.swift
//  loeffel_app
//
//  Created by Mario Hoffmann on 26.02.15.
//  Copyright (c) 2015 Mario Hoffmann. All rights reserved.
//

import UIKit

class ViewControllerHistory: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var textbox: UITextView!
    
    @IBAction func segmentChange(sender: UISegmentedControl) {
        
        switch segmentedControl.selectedSegmentIndex{
        case 0:
            textbox.text = "Die im Volksmund als \"Löffelfamilie\" bekannte Leuchtreklame des VEB Feinkost Leipzig wurde 1973 am Firmensitz in der Karl-Liebknecht-Straße errichtet. 1993 wurde sie zum Kulturdenkmal erklärt, 1999 und nochmals 2011 saniert. Seit 2008 hat der Verein Löffelfamilie e.V. die Aufgabe übernommen, die Löffelfamilie zu erhalten und zu betreiben.";
        case 1:
            textbox.text = "Abmessung am Standort (Höhe x Breite): \nca. 12m Länge x 7m Höhe\nAnzahl der offen aufgesetzten Glassysteme:\n197 Glassysteme\nInnenausleuchtung mit LSR (1999), seit 2011 mit modernen, sparsamen LED\nNeon-Glaslänge in etwa:\nfür 4 Figuren insgesamt 194m\nGlasfarben:\nBlau, Rot, Gelb, Grün, Klarglas\nTrafos:\n36 Streufeldtrafos mit Schaltwerk\nLeistung:\nca. 14 kW\nStromverbrauch:\nca. 4,1 KW/Std\nSchaltwerk über Zeitschaltuhr gesteuert und über bedarfsweise GSM-Modul einschaltbar (Spendentelefonschaltung in Vorbereitung)\n";
        default:
            break;
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
