//
//  ViewControllerFacebook.swift
//  loeffel_app
//
//  Created by Mario Hoffmann on 28.02.15.
//  Copyright (c) 2015 Mario Hoffmann. All rights reserved.
//

import UIKit

class ViewControllerFacebook: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL(string: "http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FLoeffelfamilie&width&height=500&colorscheme=light&show_faces=false&header=false&stream=true&show_border=false")
        let request = NSURLRequest(URL: url!)
        webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
