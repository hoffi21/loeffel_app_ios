//
//  ViewControllerLoeffeln.swift
//  loeffel_app
//
//  Created by Mario Hoffmann on 26.02.15.
//  Copyright (c) 2015 Mario Hoffmann. All rights reserved.
//

import UIKit

class ViewControllerLoeffeln: UIViewController {

    @IBOutlet weak var button_agb: UIButton!
    @IBOutlet weak var switch_agb: UISwitch!
    
    @IBAction func mySwitch(sender: AnyObject) {
        if switch_agb.on {
            button_agb.enabled = true;
        } else {
            button_agb.enabled = false;
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
